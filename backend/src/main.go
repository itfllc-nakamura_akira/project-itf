package main

import (
	"app"
	"app/config"
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"go.uber.org/dig"
)

// Env : 環境変数
type Env struct {
	Production bool `envconfig:"WEB_API_PRODUCTION" required:"true"`
	Port       int  `envconfig:"WEB_API_PORT" required:"true"`
}

func main() {
	var err error

	// panicを拾ってログを出力して終了する
	defer func() {
		if err := recover(); err != nil {
			log.Println("[ERROR]", err)
			log.Println("[ERROR]", "Web APIが異常終了しました。")
		}
	}()

	// 依存性の注入
	var container *dig.Container
	container, err = app.DI()

	if err != nil {
		panic(fmt.Sprintf("依存性の登録に失敗しました。\n"+
			"%s\n", err))
	}

	// サーバーの起動
	err = container.Invoke(func(c *config.Config, r *gin.Engine) {
		log.Printf("Web APIの起動(Port: %d)\n", c.API.Port)

		if err := r.Run(fmt.Sprintf(":%d", c.API.Port)); err != nil {
			panic(fmt.Sprintf("Web APIの起動に失敗しました: %s", err))
		}
	})

	if err != nil {
		panic(fmt.Sprintf("サーバーの起動に失敗しました。\n"+
			"%s\n", err))
	}
}
