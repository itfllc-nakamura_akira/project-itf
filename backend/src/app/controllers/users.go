package controllers

import (
	"app/models"
	"app/services"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Users :
type Users struct {
	usersService *services.Users
}

// GetAllUsers : 全ユーザー情報を取得する
func (users *Users) GetAllUsers(c *gin.Context) {
	var (
		response []*models.Users
		err      error
	)

	response, err = users.usersService.GetAllUsers()

	if err != nil {
		log.Printf("%s\n", err)
		c.AbortWithStatus(http.StatusInternalServerError)

		return
	}

	c.JSON(200, response)
}

// NewUsersController : api/users ルーティング初期化処理
func NewUsersController(usersService *services.Users) *Users {
	var users = Users{
		usersService: usersService,
	}

	return &users
}
