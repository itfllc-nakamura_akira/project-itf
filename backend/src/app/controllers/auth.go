package controllers

import (
	"app/config"
	"app/models"
	"app/services"
	"log"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

// Auth : ルーティング情報
type Auth struct {
	authService *services.Auth
	config      *config.Config
}

// SignIn : postされたユーザー情報の検証を行い、サイン・イン処理(Cookie書き込み)を行う
func (auth *Auth) SignIn(c *gin.Context) {
	var account string = c.PostForm("account")

	if len(account) == 0 {
		c.AbortWithStatus(http.StatusForbidden)
		log.Print("ユーザー認証に失敗しました。アカウントが入力されていません。")

		return
	}

	var password string = c.PostForm("password")

	if len(password) == 0 {
		c.AbortWithStatus(http.StatusForbidden)
		log.Printf("ユーザー認証に失敗しました(%s)。パスワードが入力されていません。", account)

		return
	}

	var (
		user *models.SignInModel
		err  error
	)

	user, err = auth.authService.SignIn(account, password)

	if err != nil {
		log.Printf("%s\n", err)
		c.AbortWithStatus(http.StatusForbidden)

		return
	}

	// Cookieに含めるJWTの生成
	var jwtCookie = jwt.MapClaims{
		"id": user.ID,
	}
	var token *jwt.Token = jwt.NewWithClaims(jwt.SigningMethodHS256, jwtCookie)
	var tokenExp time.Duration = time.Duration(60 * 60 * 24 * 14)
	token.Header["exp"] = time.Now().Add(time.Duration(time.Second * tokenExp)) // JWTの有効期限2週間
	tokenString, err := token.SignedString([]byte(auth.config.API.JWTSecret))

	// Cookieのセット
	setCookie(c, "jwt", tokenString, int(tokenExp))

	c.JSON(200, user)
}

// SignOut : サイン・アウト処理(Cookieの削除)を行う
func (auth *Auth) SignOut(c *gin.Context) {
	// Cookieの有効期限を0にする
	setCookie(c, "jwt", "", 0)

	c.JSON(200, nil)
}

// setCookie : Cookieセットの共通関数
func setCookie(c *gin.Context, name string, value string, maxAge int) {
	c.SetSameSite(http.SameSiteNoneMode)
	c.SetCookie(name, value, maxAge, "", "", false, true)
}

// NewAuthController : api/auth ルーティング初期化処理
func NewAuthController(config *config.Config, authService *services.Auth) *Auth {
	var auth = Auth{
		authService: authService,
		config:      config,
	}

	return &auth
}
