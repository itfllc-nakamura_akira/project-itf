package middlewares

import (
	"app/config"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

// Middlewares :
type Middlewares struct {
	config *config.Config
}

// AuthRequired :
func (m *Middlewares) AuthRequired() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			cookieJWT string
			err       error
		)

		cookieJWT, err = c.Cookie("jwt")

		if err != nil {
			log.Println("[ERROR]", "JWT無しでのアクセス。")
			c.AbortWithStatus(http.StatusUnauthorized)

			return
		}

		token, err := jwt.Parse(cookieJWT, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}

			return []byte(m.config.API.JWTSecret), nil
		})

		if err != nil {
			log.Println("[ERROR]", err)
			c.AbortWithStatus(http.StatusUnauthorized)

			return
		}

		// 有効期限のチェック
		var JWTExp time.Time
		JWTExp, err = time.Parse(time.RFC3339Nano, token.Header["exp"].(string))

		if err != nil || time.Now().After(JWTExp) {
			log.Println("[ERROR]", "有効期限の切れたJWTでのアクセス。")
			c.AbortWithStatus(http.StatusUnauthorized)

			return
		}

		// 署名のチェック
		if _, ok := token.Claims.(jwt.MapClaims); !ok || !token.Valid {
			log.Println("[ERROR]", "偽造されたJWTでのアクセス。")
			c.AbortWithStatus(http.StatusUnauthorized)

			return
		}

		c.Next()
	}
}

// NewMiddlewares :
func NewMiddlewares(c *config.Config) *Middlewares {
	var m = Middlewares{
		config: c,
	}

	return &m
}
