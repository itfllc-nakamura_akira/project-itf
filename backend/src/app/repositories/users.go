package repositories

import (
	"app/database"
	"app/models"
)

// Users :
type Users struct {
	db *database.DB
}

// SignIn :
func (users *Users) SignIn(account string, password string) ([]*models.SignInModel, error) {
	var (
		result = []*models.SignInModel{}
		err    error
	)

	err = users.db.Select(&result, `
        SELECT
            id,
            employee_number
        FROM
            project_itf.users
        WHERE
            account = ? AND
            password = SHA2(?, 512);
    `, account, password)

	return result, err
}

// SelectAllUsers :
func (users *Users) SelectAllUsers() ([]*models.Users, error) {
	var (
		result = []*models.Users{}
		err    error
	)

	err = users.db.Select(&result, `
        SELECT
            id,
            account,
            employee_number,
            name,
            ruby
        FROM
            project_itf.users;
    `)

	return result, err
}

// NewUsersRepository :
func NewUsersRepository(db *database.DB) *Users {
	var users = Users{
		db: db,
	}

	return &users
}
