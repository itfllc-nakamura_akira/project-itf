package config

import (
	"fmt"

	"github.com/kelseyhightower/envconfig"
)

// Config : 設定モデル
type Config struct {
	API struct {
		Production bool   `envconfig:"WEB_API_PRODUCTION" required:"true"`
		Port       int    `envconfig:"WEB_API_PORT" required:"true"`
		JWTSecret  string `envconfig:"WEB_API_JWT_SECRET" required:"true"`
	}
	DB struct {
		Host               string `envconfig:"MARIA_DB_HOST" required:"true"`
		Port               string `envconfig:"MARIA_DB_PORT" required:"true"`
		User               string `envconfig:"MARIA_DB_USER" required:"true"`
		Password           string `envconfig:"MARIA_DB_PASSWORD" required:"true"`
		Database           string `envconfig:"MARIA_DB_DATABASE" required:"true"`
		MaxOpenConnections int    `envconfig:"MARIA_DB_MAX_OPEN_CONNECTIONS" required:"true"`
	}
}

// NewConfig : 環境変数を読み込んで設定変数として返す
func NewConfig() (*Config, error) {
	var c Config

	if err := envconfig.Process("", &c); err != nil {
		return nil, fmt.Errorf("環境変数の取得に失敗しました。\n"+
			"%s\n", err)
	}

	return &c, nil
}
