package infrastructures

import (
	"app/config"
	"app/controllers"
	"app/middlewares"

	"github.com/gin-gonic/gin"
)

// NewServer :
func NewServer(
	c *config.Config,
	m *middlewares.Middlewares,
	authController *controllers.Auth,
	usersController *controllers.Users,
) *gin.Engine {
	// Ginフレームワークの初期化
	if c.API.Production {
		gin.SetMode(gin.ReleaseMode)
	}

	var r = gin.Default()

	// コントローラーの登録
	var baseURL string

	// Authコントローラー
	var authRouters = r.Group("/api/auth")
	{
		authRouters.POST("/signIn", authController.SignIn)
		authRouters.POST("/signOut", authController.SignOut)
	}

	// Usersコントローラー
	var usersRouters = r.Group("/api/users", m.AuthRequired())
	{
		usersRouters.GET(baseURL, usersController.GetAllUsers)
	}

	return r
}
