package app

import (
	"app/config"
	"app/controllers"
	"app/database"
	"app/infrastructures"
	"app/middlewares"
	"app/repositories"
	"app/services"

	"go.uber.org/dig"
)

// DI : 依存性の登録を行う
func DI() (*dig.Container, error) {
	var container *dig.Container = dig.New()
	var err error

	// config : 各種環境変数を保持する(いろんなところから参照され、どこにも依存しない)
	err = container.Provide(config.NewConfig)

	// middlewares : 外部とcontollerの処理の間に差し込む処理
	err = container.Provide(middlewares.NewMiddlewares)

	// infrastructures : 外部とcontrollerを繋ぐところ(外部 -> controllers)
	err = container.Provide(infrastructures.NewServer)

	// usecase : 外部とdatabaseの間を取り持つところ(controllers -> services -> repositories -> database)
	err = container.Provide(repositories.NewUsersRepository)
	err = container.Provide(services.NewAuthService)
	err = container.Provide(services.NewUsersService)
	err = container.Provide(controllers.NewAuthController)
	err = container.Provide(controllers.NewUsersController)

	// database : databaseにアクセスするところ
	err = container.Provide(database.NewDatabase)

	return container, err
}
