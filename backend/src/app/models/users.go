package models

// SignInModel :
type SignInModel struct {
	ID             string `db:"id" json:"id"`
	EmployeeNumber int    `db:"employee_number" json:"employeeNumber"`
}

// Users :
type Users struct {
	ID             string `db:"id" json:"id"`
	Account        string `db:"account" json:"account"`
	EmployeeNumber int    `db:"employee_number" json:"employeeNumber"`
	Name           string `db:"name" json:"name"`
	Ruby           string `db:"ruby" json:"ruby"`
}
