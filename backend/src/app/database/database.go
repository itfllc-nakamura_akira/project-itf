package database

import (
	"app/config"
	"fmt"
	"time"

	// MySQL driver
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

// DB :
type DB struct {
	connection *sqlx.DB
}

// Select :
func (db *DB) Select(dest interface{}, query string, args ...interface{}) error {
	var err error = db.connection.Select(dest, query, args...)

	if err != nil {
		return fmt.Errorf("Queryの実行に失敗しました。\n"+
			"%s\n", err)
	}

	return nil
}

// NewDatabase :
func NewDatabase(c *config.Config) (*DB, error) {
	var (
		db  = DB{}
		err error
	)

	db.connection, err = connect(c.DB.User, c.DB.Password, c.DB.Host, c.DB.Port, c.DB.Database, c.DB.MaxOpenConnections)

	if err != nil {
		return nil, fmt.Errorf("データベースコネクションの取得に失敗しました。\n"+
			"%s\n", err)
	}

	return &db, nil
}

// Connect :
func connect(user string, password string, host string, port string, database string, maxOpenConnections int) (*sqlx.DB, error) {
	var connectionStr string = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", user, password, host, port, database)
	db, err := sqlx.Connect("mysql", connectionStr)

	if err != nil {
		return nil, fmt.Errorf("データベースへの接続に失敗しました。\n"+
			"(User:%s Password:%s Host:%s Port:%s Database:%s)\n"+
			"%s\n",
			user, password, host, port, database, err)
	}

	db.SetMaxIdleConns(maxOpenConnections)
	db.SetMaxOpenConns(maxOpenConnections)
	db.SetConnMaxLifetime(time.Duration(maxOpenConnections) * time.Second)

	return db, nil
}
