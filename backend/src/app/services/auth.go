package services

import (
	"app/models"
	"app/repositories"
	"fmt"
)

// Auth :
type Auth struct {
	usersRepository *repositories.Users
}

// SignIn :
func (auth *Auth) SignIn(account string, password string) (*models.SignInModel, error) {
	var (
		users []*models.SignInModel
		err   error
	)

	users, err = auth.usersRepository.SignIn(account, password)

	if err != nil {
		return nil, err
	}

	if len(users) != 1 {
		return nil, fmt.Errorf("ユーザー認証に失敗しました(%s)。ユーザーIDかパスワードが間違っています。", account)
	}

	return users[0], nil
}

// NewAuthService :
func NewAuthService(usersRepository *repositories.Users) *Auth {
	var auth = Auth{
		usersRepository: usersRepository,
	}

	return &auth
}
