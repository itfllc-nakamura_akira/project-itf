package services

import (
	"app/models"
	"app/repositories"
)

// Users :
type Users struct {
	usersRepository *repositories.Users
}

// GetAllUsers :
func (users *Users) GetAllUsers() ([]*models.Users, error) {
	var (
		result []*models.Users
		err    error
	)

	result, err = users.usersRepository.SelectAllUsers()

	if err != nil {
		return nil, err
	}

	return result, nil
}

// NewUsersService :
func NewUsersService(usersRepository *repositories.Users) *Users {
	var users = Users{
		usersRepository: usersRepository,
	}

	return &users
}
