SET CHARACTER_SET_CLIENT = utf8mb4;
SET CHARACTER_SET_CONNECTION = utf8mb4;

INSERT INTO
    project_itf.users (id, account, password, employee_number, name, ruby)
VALUES
    ('01E36QR9BPWW0GWMYMDNG2PPJG', 'yamanouchi.takahiko@itfllc.co.jp', SHA2('yamanouchi.takahiko', 512), 1, '山之内貴彦', 'ヤマノウチタカヒコ'),
    ('01E36QRF4DSV48FXV27WQ4GP4T', 'ogachi.satoshi@itfllc.co.jp', SHA2('ogachi.satoshi', 512), 3, '大勝聡', 'オオガチサトシ'),
    ('01E36QRKENV0E2EA3GQBHBF30G', 'tanaka.youichi@itfllc.co.jp', SHA2('tanaka.youichi', 512), 4, '田中陽一', 'タナカヨウイチ'),
    ('01E36QRPQMN6PD50M8JCM5Z4SF', 'matsuo.yusuke@itfllc.co.jp', SHA2('matsuo.yusuke', 512), 5, '松尾祐介', 'マツオユウスケ'),
    ('01E36QRRSP6Z6TYGRYPZ9K96F5', 'nakamura.akira@itfllc.co.jp', SHA2('nakamura.akira', 512), 6, '中村輝', 'ナカムラアキラ'),
    ('01E36QRTJCDQRQATG1EN9V53QH', 'ishibashi.sachiyo@itfllc.co.jp', SHA2('ishibashi.sachiyo', 512), 7, '石橋祥世', 'イシバシサチヨ'),
    ('01E36QRW35F1WXSCY9X888GTZN', 'umezaki.takuya@itfllc.co.jp', SHA2('umezaki.takuya', 512), 8, '梅﨑拓哉', 'ウメザキタクヤ'),
    ('01E36QRXJM4ZNZJC1A3B7NHKJ8', 'kobayashi.takashi@itfllc.co.jp', SHA2('kobayashi.takashi', 512), 9, '小林高士', 'コバヤシタカシ'),
    ('01E36QRZ460X2AKZ5CMWPGNAM8', 'higuchi.yoshimasa@itfllc.co.jp', SHA2('higuchi.yoshimasa', 512), 10, '樋口義昌', 'ヒグチヨシマサ'),
    ('01E36QS4DY4JH98PVTGRKVN3R3', 'yasuda.masahiko@itfllc.co.jp', SHA2('yasuda.masahiko', 512), 11, '安田政彦', 'ヤスダマサヒコ'),
    ('01E36QS6R5Z4RS7Q9H0ECT9C0J', 'shimose.hiroyuki@itfllc.co.jp', SHA2('shimose.hiroyuki', 512), 13, '下瀬裕之', 'シモセヒロユキ'),
    ('01E36QS8JYE7BP6D6ZD324STVM', 'sakamoto.mitsuaki@itfllc.co.jp', SHA2('sakamoto.mitsuaki', 512), 14, '坂本光章', 'サカモトミツアキ'),
    ('01E36QSA84G8N4RRV8WR453RA2', 'kusunoki.kouki@itfllc.co.jp', SHA2('kusunoki.kouki', 512), 17, '楠光希', 'クスノキコウキ'),
    ('01E36QSEXWN1B2BHEDHPQJS5YE', 'wada.tsubasa@itfllc.co.jp', SHA2('wada.tsubasa', 512), 19, '和田翼', 'ワダツバサ'),
    ('01E36QSGNGBT3FT1VVKKXVHQES', 'imamura.tomu@itfllc.co.jp', SHA2('imamura.tomu', 512), 20, '今村音椋', 'イマムラトム'),
    ('01E36QSJ5NDZR2EMQEP696WNS0', 'fukuki.hikaru@itfllc.co.jp', SHA2('fukuki.hikaru', 512), 21, '福木ひかる', 'フクキヒカル'),
    ('01E36QSKS5TB21754GYQHXN0MM', 'ejima.kaori@itfllc.co.jp', SHA2('ejima.kaori', 512), 22, '江嶋華織', 'エジマカオリ'),
    ('01E36QSNBD6AMS114HFP0M9NMP', 'yokoyama.daiki@itfllc.co.jp', SHA2('yokoyama.daiki', 512), 23, '横山大樹', 'ヨコヤマダイキ'),
    ('01E36QSPYN02V4KYGZMKTRBQX4', 'ando.hirotaka@itfllc.co.jp', SHA2('ando.hirotaka', 512), 24, '安藤博隆', 'アンドウヒロタカ'),
    ('01E36QSRHMEJZR5QEJGPMFFXKQ', 'doizaki.fukumi@itfllc.co.jp', SHA2('doizaki.fukumi', 512), 25, '土斐崎福実', 'ドイザキフクミ'),
    ('01E36QSSZD5EBTGWW9BXV1C7PD', 'horinouchi.ryuichi@itfllc.co.jp', SHA2('horinouchi.ryuichi', 512), 26, '堀之内龍一', 'ホリノウチリュウイチ')
;





















