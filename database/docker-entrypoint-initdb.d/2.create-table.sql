﻿SET CHARACTER_SET_CLIENT = utf8mb4;
SET CHARACTER_SET_CONNECTION = utf8mb4;

-- Project Name : project-itf
-- Date/Time    : 2020/03/12 15:26:12
-- Author       : itfllc
-- RDBMS Type   : MySQL
-- Application  : A5:SQL Mk-2

-- ユーザー
drop table if exists project_itf.users cascade;

create table project_itf.users (
  id varchar(32) not null comment 'ID'
  , account text not null comment 'アカウント:メールアドレス'
  , password text not null comment 'パスワード'
  , employee_number smallint unsigned not null comment '社員番号'
  , name text not null comment '名前'
  , ruby text not null comment 'フリガナ'
  , constraint users_PKC primary key (id)
) comment 'ユーザー' ;

alter table project_itf.users add unique users_IX1 (account) ;
